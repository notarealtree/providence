package xyz.notarealtree.suddenly.freight.core.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.time.LocalDateTime;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

/**
 * Immutable implementation of {@link Metadata}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableMetadata.builder()}.
 */
@SuppressWarnings({"all"})
@ParametersAreNonnullByDefault
@Generated({"Immutables.generator", "Metadata"})
@Immutable
public final class ImmutableMetadata implements Metadata {
  private final String _id;
  private final LocalDateTime date;

  private ImmutableMetadata(String _id, LocalDateTime date) {
    this._id = _id;
    this.date = date;
  }

  /**
   * @return The value of the {@code _id} attribute
   */
  @Override
  public String _id() {
    return _id;
  }

  /**
   * @return The value of the {@code date} attribute
   */
  @Override
  public LocalDateTime getDate() {
    return date;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Metadata#_id() _id} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for _id
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMetadata with_id(String value) {
    if (this._id.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "_id");
    return new ImmutableMetadata(newValue, this.date);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Metadata#getDate() date} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for date
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableMetadata withDate(LocalDateTime value) {
    if (this.date == value) return this;
    LocalDateTime newValue = Preconditions.checkNotNull(value, "date");
    return new ImmutableMetadata(this._id, newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableMetadata} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(@Nullable Object another) {
    if (this == another) return true;
    return another instanceof ImmutableMetadata
        && equalTo((ImmutableMetadata) another);
  }

  private boolean equalTo(ImmutableMetadata another) {
    return _id.equals(another._id)
        && date.equals(another.date);
  }

  /**
   * Computes a hash code from attributes: {@code _id}, {@code date}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + _id.hashCode();
    h += (h << 5) + date.hashCode();
    return h;
  }

  /**
   * Prints the immutable value {@code Metadata} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper("Metadata")
        .omitNullValues()
        .add("_id", _id)
        .add("date", date)
        .toString();
  }

  /**
   * Creates an immutable copy of a {@link Metadata} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable Metadata instance
   */
  public static ImmutableMetadata copyOf(Metadata instance) {
    if (instance instanceof ImmutableMetadata) {
      return (ImmutableMetadata) instance;
    }
    return ImmutableMetadata.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link ImmutableMetadata ImmutableMetadata}.
   * @return A new ImmutableMetadata builder
   */
  public static ImmutableMetadata.Builder builder() {
    return new ImmutableMetadata.Builder();
  }

  /**
   * Builds instances of type {@link ImmutableMetadata ImmutableMetadata}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @NotThreadSafe
  public static final class Builder {
    private static final long INIT_BIT__ID = 0x1L;
    private static final long INIT_BIT_DATE = 0x2L;
    private long initBits = 0x3L;

    private @Nullable String _id;
    private @Nullable LocalDateTime date;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code Metadata} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(Metadata instance) {
      Preconditions.checkNotNull(instance, "instance");
      _id(instance._id());
      date(instance.getDate());
      return this;
    }

    /**
     * Initializes the value for the {@link Metadata#_id() _id} attribute.
     * @param _id The value for _id 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder _id(String _id) {
      this._id = Preconditions.checkNotNull(_id, "_id");
      initBits &= ~INIT_BIT__ID;
      return this;
    }

    /**
     * Initializes the value for the {@link Metadata#getDate() date} attribute.
     * @param date The value for date 
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder date(LocalDateTime date) {
      this.date = Preconditions.checkNotNull(date, "date");
      initBits &= ~INIT_BIT_DATE;
      return this;
    }

    /**
     * Builds a new {@link ImmutableMetadata ImmutableMetadata}.
     * @return An immutable instance of Metadata
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableMetadata build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutableMetadata(_id, date);
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = Lists.newArrayList();
      if ((initBits & INIT_BIT__ID) != 0) attributes.add("_id");
      if ((initBits & INIT_BIT_DATE) != 0) attributes.add("date");
      return "Cannot build Metadata, some of required attributes are not set " + attributes;
    }
  }
}
