package xyz.notarealtree.suddenly.freight.core.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

/**
 * Immutable implementation of {@link Contract}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableContract.builder()}.
 */
@SuppressWarnings({"all"})
@ParametersAreNonnullByDefault
@Generated({"Immutables.generator", "Contract"})
@Immutable
public final class ImmutableContract implements Contract {
  private final Long contractID;
  private final String issuerID;
  private final String issuerCorpID;
  private final String assigneeID;
  private final String acceptorID;
  private final String startStationID;
  private final String endStationID;
  private final String type;
  private final String status;
  private final String title;
  private final String forCorp;
  private final String availability;
  private final String dateIssued;
  private final String dateExpired;
  private final String dateAccepted;
  private final String dateCompleted;
  private final Integer numDays;
  private final Double price;
  private final Double reward;
  private final Double collateral;
  private final Double buyout;
  private final Double volume;

  private ImmutableContract(
      Long contractID,
      String issuerID,
      String issuerCorpID,
      String assigneeID,
      String acceptorID,
      String startStationID,
      String endStationID,
      String type,
      String status,
      String title,
      String forCorp,
      String availability,
      String dateIssued,
      String dateExpired,
      String dateAccepted,
      String dateCompleted,
      Integer numDays,
      Double price,
      Double reward,
      Double collateral,
      Double buyout,
      Double volume) {
    this.contractID = contractID;
    this.issuerID = issuerID;
    this.issuerCorpID = issuerCorpID;
    this.assigneeID = assigneeID;
    this.acceptorID = acceptorID;
    this.startStationID = startStationID;
    this.endStationID = endStationID;
    this.type = type;
    this.status = status;
    this.title = title;
    this.forCorp = forCorp;
    this.availability = availability;
    this.dateIssued = dateIssued;
    this.dateExpired = dateExpired;
    this.dateAccepted = dateAccepted;
    this.dateCompleted = dateCompleted;
    this.numDays = numDays;
    this.price = price;
    this.reward = reward;
    this.collateral = collateral;
    this.buyout = buyout;
    this.volume = volume;
  }

  /**
   * @return The value of the {@code contractID} attribute
   */
  @JsonProperty("contractID")
  @Override
  public Long contractID() {
    return contractID;
  }

  /**
   * @return The value of the {@code issuerID} attribute
   */
  @JsonProperty("issuerID")
  @Override
  public String issuerID() {
    return issuerID;
  }

  /**
   * @return The value of the {@code issuerCorpID} attribute
   */
  @JsonProperty("issuerCorpID")
  @Override
  public String issuerCorpID() {
    return issuerCorpID;
  }

  /**
   * @return The value of the {@code assigneeID} attribute
   */
  @JsonProperty("assigneeID")
  @Override
  public String assigneeID() {
    return assigneeID;
  }

  /**
   * @return The value of the {@code acceptorID} attribute
   */
  @JsonProperty("acceptorID")
  @Override
  public String acceptorID() {
    return acceptorID;
  }

  /**
   * @return The value of the {@code startStationID} attribute
   */
  @JsonProperty("startStationID")
  @Override
  public String startStationID() {
    return startStationID;
  }

  /**
   * @return The value of the {@code endStationID} attribute
   */
  @JsonProperty("endStationID")
  @Override
  public String endStationID() {
    return endStationID;
  }

  /**
   * @return The value of the {@code type} attribute
   */
  @JsonProperty("type")
  @Override
  public String type() {
    return type;
  }

  /**
   * @return The value of the {@code status} attribute
   */
  @JsonProperty("status")
  @Override
  public String status() {
    return status;
  }

  /**
   * @return The value of the {@code title} attribute
   */
  @JsonProperty("title")
  @Override
  public String title() {
    return title;
  }

  /**
   * @return The value of the {@code forCorp} attribute
   */
  @JsonProperty("forCorp")
  @Override
  public String forCorp() {
    return forCorp;
  }

  /**
   * @return The value of the {@code availability} attribute
   */
  @JsonProperty("availability")
  @Override
  public String availability() {
    return availability;
  }

  /**
   * @return The value of the {@code dateIssued} attribute
   */
  @JsonProperty("dateIssued")
  @Override
  public String dateIssued() {
    return dateIssued;
  }

  /**
   * @return The value of the {@code dateExpired} attribute
   */
  @JsonProperty("dateExpired")
  @Override
  public String dateExpired() {
    return dateExpired;
  }

  /**
   * @return The value of the {@code dateAccepted} attribute
   */
  @JsonProperty("dateAccepted")
  @Override
  public String dateAccepted() {
    return dateAccepted;
  }

  /**
   * @return The value of the {@code dateCompleted} attribute
   */
  @JsonProperty("dateCompleted")
  @Override
  public String dateCompleted() {
    return dateCompleted;
  }

  /**
   * @return The value of the {@code numDays} attribute
   */
  @JsonProperty("numDays")
  @Override
  public Integer numDays() {
    return numDays;
  }

  /**
   * @return The value of the {@code price} attribute
   */
  @JsonProperty("price")
  @Override
  public Double price() {
    return price;
  }

  /**
   * @return The value of the {@code reward} attribute
   */
  @JsonProperty("reward")
  @Override
  public Double reward() {
    return reward;
  }

  /**
   * @return The value of the {@code collateral} attribute
   */
  @JsonProperty("collateral")
  @Override
  public Double collateral() {
    return collateral;
  }

  /**
   * @return The value of the {@code buyout} attribute
   */
  @JsonProperty("buyout")
  @Override
  public Double buyout() {
    return buyout;
  }

  /**
   * @return The value of the {@code volume} attribute
   */
  @JsonProperty("volume")
  @Override
  public Double volume() {
    return volume;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#contractID() contractID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for contractID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withContractID(Long value) {
    if (this.contractID.equals(value)) return this;
    Long newValue = Preconditions.checkNotNull(value, "contractID");
    return new ImmutableContract(
        newValue,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#issuerID() issuerID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for issuerID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withIssuerID(String value) {
    if (this.issuerID.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "issuerID");
    return new ImmutableContract(
        this.contractID,
        newValue,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#issuerCorpID() issuerCorpID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for issuerCorpID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withIssuerCorpID(String value) {
    if (this.issuerCorpID.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "issuerCorpID");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        newValue,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#assigneeID() assigneeID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for assigneeID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withAssigneeID(String value) {
    if (this.assigneeID.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "assigneeID");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        newValue,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#acceptorID() acceptorID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for acceptorID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withAcceptorID(String value) {
    if (this.acceptorID.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "acceptorID");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        newValue,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#startStationID() startStationID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for startStationID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withStartStationID(String value) {
    if (this.startStationID.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "startStationID");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        newValue,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#endStationID() endStationID} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for endStationID
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withEndStationID(String value) {
    if (this.endStationID.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "endStationID");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        newValue,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#type() type} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for type
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withType(String value) {
    if (this.type.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "type");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        newValue,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#status() status} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for status
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withStatus(String value) {
    if (this.status.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "status");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        newValue,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#title() title} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for title
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withTitle(String value) {
    if (this.title.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "title");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        newValue,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#forCorp() forCorp} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for forCorp
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withForCorp(String value) {
    if (this.forCorp.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "forCorp");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        newValue,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#availability() availability} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for availability
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withAvailability(String value) {
    if (this.availability.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "availability");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        newValue,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#dateIssued() dateIssued} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for dateIssued
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withDateIssued(String value) {
    if (this.dateIssued.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "dateIssued");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        newValue,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#dateExpired() dateExpired} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for dateExpired
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withDateExpired(String value) {
    if (this.dateExpired.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "dateExpired");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        newValue,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#dateAccepted() dateAccepted} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for dateAccepted
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withDateAccepted(String value) {
    if (this.dateAccepted.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "dateAccepted");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        newValue,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#dateCompleted() dateCompleted} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for dateCompleted
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withDateCompleted(String value) {
    if (this.dateCompleted.equals(value)) return this;
    String newValue = Preconditions.checkNotNull(value, "dateCompleted");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        newValue,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#numDays() numDays} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for numDays
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withNumDays(Integer value) {
    if (this.numDays.equals(value)) return this;
    Integer newValue = Preconditions.checkNotNull(value, "numDays");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        newValue,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#price() price} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for price
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withPrice(Double value) {
    if (this.price.equals(value)) return this;
    Double newValue = Preconditions.checkNotNull(value, "price");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        newValue,
        this.reward,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#reward() reward} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for reward
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withReward(Double value) {
    if (this.reward.equals(value)) return this;
    Double newValue = Preconditions.checkNotNull(value, "reward");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        newValue,
        this.collateral,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#collateral() collateral} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for collateral
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withCollateral(Double value) {
    if (this.collateral.equals(value)) return this;
    Double newValue = Preconditions.checkNotNull(value, "collateral");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        newValue,
        this.buyout,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#buyout() buyout} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for buyout
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withBuyout(Double value) {
    if (this.buyout.equals(value)) return this;
    Double newValue = Preconditions.checkNotNull(value, "buyout");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        newValue,
        this.volume);
  }

  /**
   * Copy the current immutable object by setting a value for the {@link Contract#volume() volume} attribute.
   * An equals check used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for volume
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContract withVolume(Double value) {
    if (this.volume.equals(value)) return this;
    Double newValue = Preconditions.checkNotNull(value, "volume");
    return new ImmutableContract(
        this.contractID,
        this.issuerID,
        this.issuerCorpID,
        this.assigneeID,
        this.acceptorID,
        this.startStationID,
        this.endStationID,
        this.type,
        this.status,
        this.title,
        this.forCorp,
        this.availability,
        this.dateIssued,
        this.dateExpired,
        this.dateAccepted,
        this.dateCompleted,
        this.numDays,
        this.price,
        this.reward,
        this.collateral,
        this.buyout,
        newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableContract} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(@Nullable Object another) {
    if (this == another) return true;
    return another instanceof ImmutableContract
        && equalTo((ImmutableContract) another);
  }

  private boolean equalTo(ImmutableContract another) {
    return contractID.equals(another.contractID)
        && issuerID.equals(another.issuerID)
        && issuerCorpID.equals(another.issuerCorpID)
        && assigneeID.equals(another.assigneeID)
        && acceptorID.equals(another.acceptorID)
        && startStationID.equals(another.startStationID)
        && endStationID.equals(another.endStationID)
        && type.equals(another.type)
        && status.equals(another.status)
        && title.equals(another.title)
        && forCorp.equals(another.forCorp)
        && availability.equals(another.availability)
        && dateIssued.equals(another.dateIssued)
        && dateExpired.equals(another.dateExpired)
        && dateAccepted.equals(another.dateAccepted)
        && dateCompleted.equals(another.dateCompleted)
        && numDays.equals(another.numDays)
        && price.equals(another.price)
        && reward.equals(another.reward)
        && collateral.equals(another.collateral)
        && buyout.equals(another.buyout)
        && volume.equals(another.volume);
  }

  /**
   * Computes a hash code from attributes: {@code contractID}, {@code issuerID}, {@code issuerCorpID}, {@code assigneeID}, {@code acceptorID}, {@code startStationID}, {@code endStationID}, {@code type}, {@code status}, {@code title}, {@code forCorp}, {@code availability}, {@code dateIssued}, {@code dateExpired}, {@code dateAccepted}, {@code dateCompleted}, {@code numDays}, {@code price}, {@code reward}, {@code collateral}, {@code buyout}, {@code volume}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + contractID.hashCode();
    h += (h << 5) + issuerID.hashCode();
    h += (h << 5) + issuerCorpID.hashCode();
    h += (h << 5) + assigneeID.hashCode();
    h += (h << 5) + acceptorID.hashCode();
    h += (h << 5) + startStationID.hashCode();
    h += (h << 5) + endStationID.hashCode();
    h += (h << 5) + type.hashCode();
    h += (h << 5) + status.hashCode();
    h += (h << 5) + title.hashCode();
    h += (h << 5) + forCorp.hashCode();
    h += (h << 5) + availability.hashCode();
    h += (h << 5) + dateIssued.hashCode();
    h += (h << 5) + dateExpired.hashCode();
    h += (h << 5) + dateAccepted.hashCode();
    h += (h << 5) + dateCompleted.hashCode();
    h += (h << 5) + numDays.hashCode();
    h += (h << 5) + price.hashCode();
    h += (h << 5) + reward.hashCode();
    h += (h << 5) + collateral.hashCode();
    h += (h << 5) + buyout.hashCode();
    h += (h << 5) + volume.hashCode();
    return h;
  }

  /**
   * Prints the immutable value {@code Contract} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper("Contract")
        .omitNullValues()
        .add("contractID", contractID)
        .add("issuerID", issuerID)
        .add("issuerCorpID", issuerCorpID)
        .add("assigneeID", assigneeID)
        .add("acceptorID", acceptorID)
        .add("startStationID", startStationID)
        .add("endStationID", endStationID)
        .add("type", type)
        .add("status", status)
        .add("title", title)
        .add("forCorp", forCorp)
        .add("availability", availability)
        .add("dateIssued", dateIssued)
        .add("dateExpired", dateExpired)
        .add("dateAccepted", dateAccepted)
        .add("dateCompleted", dateCompleted)
        .add("numDays", numDays)
        .add("price", price)
        .add("reward", reward)
        .add("collateral", collateral)
        .add("buyout", buyout)
        .add("volume", volume)
        .toString();
  }

  /**
   * Utility type used to correctly read immutable object from JSON representation.
   * @deprecated Do not use this type directly, it exists only for the <em>Jackson</em>-binding infrastructure
   */
  @Deprecated
  @JsonDeserialize
  @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
  static final class Json implements Contract {
    @Nullable Long contractID;
    @Nullable String issuerID;
    @Nullable String issuerCorpID;
    @Nullable String assigneeID;
    @Nullable String acceptorID;
    @Nullable String startStationID;
    @Nullable String endStationID;
    @Nullable String type;
    @Nullable String status;
    @Nullable String title;
    @Nullable String forCorp;
    @Nullable String availability;
    @Nullable String dateIssued;
    @Nullable String dateExpired;
    @Nullable String dateAccepted;
    @Nullable String dateCompleted;
    @Nullable Integer numDays;
    @Nullable Double price;
    @Nullable Double reward;
    @Nullable Double collateral;
    @Nullable Double buyout;
    @Nullable Double volume;
    @JsonProperty("contractID")
    public void setContractID(Long contractID) {
      this.contractID = contractID;
    }
    @JsonProperty("issuerID")
    public void setIssuerID(String issuerID) {
      this.issuerID = issuerID;
    }
    @JsonProperty("issuerCorpID")
    public void setIssuerCorpID(String issuerCorpID) {
      this.issuerCorpID = issuerCorpID;
    }
    @JsonProperty("assigneeID")
    public void setAssigneeID(String assigneeID) {
      this.assigneeID = assigneeID;
    }
    @JsonProperty("acceptorID")
    public void setAcceptorID(String acceptorID) {
      this.acceptorID = acceptorID;
    }
    @JsonProperty("startStationID")
    public void setStartStationID(String startStationID) {
      this.startStationID = startStationID;
    }
    @JsonProperty("endStationID")
    public void setEndStationID(String endStationID) {
      this.endStationID = endStationID;
    }
    @JsonProperty("type")
    public void setType(String type) {
      this.type = type;
    }
    @JsonProperty("status")
    public void setStatus(String status) {
      this.status = status;
    }
    @JsonProperty("title")
    public void setTitle(String title) {
      this.title = title;
    }
    @JsonProperty("forCorp")
    public void setForCorp(String forCorp) {
      this.forCorp = forCorp;
    }
    @JsonProperty("availability")
    public void setAvailability(String availability) {
      this.availability = availability;
    }
    @JsonProperty("dateIssued")
    public void setDateIssued(String dateIssued) {
      this.dateIssued = dateIssued;
    }
    @JsonProperty("dateExpired")
    public void setDateExpired(String dateExpired) {
      this.dateExpired = dateExpired;
    }
    @JsonProperty("dateAccepted")
    public void setDateAccepted(String dateAccepted) {
      this.dateAccepted = dateAccepted;
    }
    @JsonProperty("dateCompleted")
    public void setDateCompleted(String dateCompleted) {
      this.dateCompleted = dateCompleted;
    }
    @JsonProperty("numDays")
    public void setNumDays(Integer numDays) {
      this.numDays = numDays;
    }
    @JsonProperty("price")
    public void setPrice(Double price) {
      this.price = price;
    }
    @JsonProperty("reward")
    public void setReward(Double reward) {
      this.reward = reward;
    }
    @JsonProperty("collateral")
    public void setCollateral(Double collateral) {
      this.collateral = collateral;
    }
    @JsonProperty("buyout")
    public void setBuyout(Double buyout) {
      this.buyout = buyout;
    }
    @JsonProperty("volume")
    public void setVolume(Double volume) {
      this.volume = volume;
    }
    @Override
    public Long contractID() { throw new UnsupportedOperationException(); }
    @Override
    public String issuerID() { throw new UnsupportedOperationException(); }
    @Override
    public String issuerCorpID() { throw new UnsupportedOperationException(); }
    @Override
    public String assigneeID() { throw new UnsupportedOperationException(); }
    @Override
    public String acceptorID() { throw new UnsupportedOperationException(); }
    @Override
    public String startStationID() { throw new UnsupportedOperationException(); }
    @Override
    public String endStationID() { throw new UnsupportedOperationException(); }
    @Override
    public String type() { throw new UnsupportedOperationException(); }
    @Override
    public String status() { throw new UnsupportedOperationException(); }
    @Override
    public String title() { throw new UnsupportedOperationException(); }
    @Override
    public String forCorp() { throw new UnsupportedOperationException(); }
    @Override
    public String availability() { throw new UnsupportedOperationException(); }
    @Override
    public String dateIssued() { throw new UnsupportedOperationException(); }
    @Override
    public String dateExpired() { throw new UnsupportedOperationException(); }
    @Override
    public String dateAccepted() { throw new UnsupportedOperationException(); }
    @Override
    public String dateCompleted() { throw new UnsupportedOperationException(); }
    @Override
    public Integer numDays() { throw new UnsupportedOperationException(); }
    @Override
    public Double price() { throw new UnsupportedOperationException(); }
    @Override
    public Double reward() { throw new UnsupportedOperationException(); }
    @Override
    public Double collateral() { throw new UnsupportedOperationException(); }
    @Override
    public Double buyout() { throw new UnsupportedOperationException(); }
    @Override
    public Double volume() { throw new UnsupportedOperationException(); }
  }

  /**
   * @param json A JSON-bindable data structure
   * @return An immutable value type
   * @deprecated Do not use this method directly, it exists only for the <em>Jackson</em>-binding infrastructure
   */
  @Deprecated
  @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
  static ImmutableContract fromJson(Json json) {
    ImmutableContract.Builder builder = ImmutableContract.builder();
    if (json.contractID != null) {
      builder.contractID(json.contractID);
    }
    if (json.issuerID != null) {
      builder.issuerID(json.issuerID);
    }
    if (json.issuerCorpID != null) {
      builder.issuerCorpID(json.issuerCorpID);
    }
    if (json.assigneeID != null) {
      builder.assigneeID(json.assigneeID);
    }
    if (json.acceptorID != null) {
      builder.acceptorID(json.acceptorID);
    }
    if (json.startStationID != null) {
      builder.startStationID(json.startStationID);
    }
    if (json.endStationID != null) {
      builder.endStationID(json.endStationID);
    }
    if (json.type != null) {
      builder.type(json.type);
    }
    if (json.status != null) {
      builder.status(json.status);
    }
    if (json.title != null) {
      builder.title(json.title);
    }
    if (json.forCorp != null) {
      builder.forCorp(json.forCorp);
    }
    if (json.availability != null) {
      builder.availability(json.availability);
    }
    if (json.dateIssued != null) {
      builder.dateIssued(json.dateIssued);
    }
    if (json.dateExpired != null) {
      builder.dateExpired(json.dateExpired);
    }
    if (json.dateAccepted != null) {
      builder.dateAccepted(json.dateAccepted);
    }
    if (json.dateCompleted != null) {
      builder.dateCompleted(json.dateCompleted);
    }
    if (json.numDays != null) {
      builder.numDays(json.numDays);
    }
    if (json.price != null) {
      builder.price(json.price);
    }
    if (json.reward != null) {
      builder.reward(json.reward);
    }
    if (json.collateral != null) {
      builder.collateral(json.collateral);
    }
    if (json.buyout != null) {
      builder.buyout(json.buyout);
    }
    if (json.volume != null) {
      builder.volume(json.volume);
    }
    return builder.build();
  }

  /**
   * Creates an immutable copy of a {@link Contract} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable Contract instance
   */
  public static ImmutableContract copyOf(Contract instance) {
    if (instance instanceof ImmutableContract) {
      return (ImmutableContract) instance;
    }
    return ImmutableContract.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link ImmutableContract ImmutableContract}.
   * @return A new ImmutableContract builder
   */
  public static ImmutableContract.Builder builder() {
    return new ImmutableContract.Builder();
  }

  /**
   * Builds instances of type {@link ImmutableContract ImmutableContract}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @NotThreadSafe
  public static final class Builder {
    private static final long INIT_BIT_CONTRACT_I_D = 0x1L;
    private static final long INIT_BIT_ISSUER_I_D = 0x2L;
    private static final long INIT_BIT_ISSUER_CORP_I_D = 0x4L;
    private static final long INIT_BIT_ASSIGNEE_I_D = 0x8L;
    private static final long INIT_BIT_ACCEPTOR_I_D = 0x10L;
    private static final long INIT_BIT_START_STATION_I_D = 0x20L;
    private static final long INIT_BIT_END_STATION_I_D = 0x40L;
    private static final long INIT_BIT_TYPE = 0x80L;
    private static final long INIT_BIT_STATUS = 0x100L;
    private static final long INIT_BIT_TITLE = 0x200L;
    private static final long INIT_BIT_FOR_CORP = 0x400L;
    private static final long INIT_BIT_AVAILABILITY = 0x800L;
    private static final long INIT_BIT_DATE_ISSUED = 0x1000L;
    private static final long INIT_BIT_DATE_EXPIRED = 0x2000L;
    private static final long INIT_BIT_DATE_ACCEPTED = 0x4000L;
    private static final long INIT_BIT_DATE_COMPLETED = 0x8000L;
    private static final long INIT_BIT_NUM_DAYS = 0x10000L;
    private static final long INIT_BIT_PRICE = 0x20000L;
    private static final long INIT_BIT_REWARD = 0x40000L;
    private static final long INIT_BIT_COLLATERAL = 0x80000L;
    private static final long INIT_BIT_BUYOUT = 0x100000L;
    private static final long INIT_BIT_VOLUME = 0x200000L;
    private long initBits = 0x3fffffL;

    private @Nullable Long contractID;
    private @Nullable String issuerID;
    private @Nullable String issuerCorpID;
    private @Nullable String assigneeID;
    private @Nullable String acceptorID;
    private @Nullable String startStationID;
    private @Nullable String endStationID;
    private @Nullable String type;
    private @Nullable String status;
    private @Nullable String title;
    private @Nullable String forCorp;
    private @Nullable String availability;
    private @Nullable String dateIssued;
    private @Nullable String dateExpired;
    private @Nullable String dateAccepted;
    private @Nullable String dateCompleted;
    private @Nullable Integer numDays;
    private @Nullable Double price;
    private @Nullable Double reward;
    private @Nullable Double collateral;
    private @Nullable Double buyout;
    private @Nullable Double volume;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code Contract} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(Contract instance) {
      Preconditions.checkNotNull(instance, "instance");
      contractID(instance.contractID());
      issuerID(instance.issuerID());
      issuerCorpID(instance.issuerCorpID());
      assigneeID(instance.assigneeID());
      acceptorID(instance.acceptorID());
      startStationID(instance.startStationID());
      endStationID(instance.endStationID());
      type(instance.type());
      status(instance.status());
      title(instance.title());
      forCorp(instance.forCorp());
      availability(instance.availability());
      dateIssued(instance.dateIssued());
      dateExpired(instance.dateExpired());
      dateAccepted(instance.dateAccepted());
      dateCompleted(instance.dateCompleted());
      numDays(instance.numDays());
      price(instance.price());
      reward(instance.reward());
      collateral(instance.collateral());
      buyout(instance.buyout());
      volume(instance.volume());
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#contractID() contractID} attribute.
     * @param contractID The value for contractID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("contractID")
    public final Builder contractID(Long contractID) {
      this.contractID = Preconditions.checkNotNull(contractID, "contractID");
      initBits &= ~INIT_BIT_CONTRACT_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#issuerID() issuerID} attribute.
     * @param issuerID The value for issuerID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("issuerID")
    public final Builder issuerID(String issuerID) {
      this.issuerID = Preconditions.checkNotNull(issuerID, "issuerID");
      initBits &= ~INIT_BIT_ISSUER_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#issuerCorpID() issuerCorpID} attribute.
     * @param issuerCorpID The value for issuerCorpID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("issuerCorpID")
    public final Builder issuerCorpID(String issuerCorpID) {
      this.issuerCorpID = Preconditions.checkNotNull(issuerCorpID, "issuerCorpID");
      initBits &= ~INIT_BIT_ISSUER_CORP_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#assigneeID() assigneeID} attribute.
     * @param assigneeID The value for assigneeID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("assigneeID")
    public final Builder assigneeID(String assigneeID) {
      this.assigneeID = Preconditions.checkNotNull(assigneeID, "assigneeID");
      initBits &= ~INIT_BIT_ASSIGNEE_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#acceptorID() acceptorID} attribute.
     * @param acceptorID The value for acceptorID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("acceptorID")
    public final Builder acceptorID(String acceptorID) {
      this.acceptorID = Preconditions.checkNotNull(acceptorID, "acceptorID");
      initBits &= ~INIT_BIT_ACCEPTOR_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#startStationID() startStationID} attribute.
     * @param startStationID The value for startStationID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("startStationID")
    public final Builder startStationID(String startStationID) {
      this.startStationID = Preconditions.checkNotNull(startStationID, "startStationID");
      initBits &= ~INIT_BIT_START_STATION_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#endStationID() endStationID} attribute.
     * @param endStationID The value for endStationID 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("endStationID")
    public final Builder endStationID(String endStationID) {
      this.endStationID = Preconditions.checkNotNull(endStationID, "endStationID");
      initBits &= ~INIT_BIT_END_STATION_I_D;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#type() type} attribute.
     * @param type The value for type 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("type")
    public final Builder type(String type) {
      this.type = Preconditions.checkNotNull(type, "type");
      initBits &= ~INIT_BIT_TYPE;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#status() status} attribute.
     * @param status The value for status 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("status")
    public final Builder status(String status) {
      this.status = Preconditions.checkNotNull(status, "status");
      initBits &= ~INIT_BIT_STATUS;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#title() title} attribute.
     * @param title The value for title 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("title")
    public final Builder title(String title) {
      this.title = Preconditions.checkNotNull(title, "title");
      initBits &= ~INIT_BIT_TITLE;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#forCorp() forCorp} attribute.
     * @param forCorp The value for forCorp 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("forCorp")
    public final Builder forCorp(String forCorp) {
      this.forCorp = Preconditions.checkNotNull(forCorp, "forCorp");
      initBits &= ~INIT_BIT_FOR_CORP;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#availability() availability} attribute.
     * @param availability The value for availability 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("availability")
    public final Builder availability(String availability) {
      this.availability = Preconditions.checkNotNull(availability, "availability");
      initBits &= ~INIT_BIT_AVAILABILITY;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#dateIssued() dateIssued} attribute.
     * @param dateIssued The value for dateIssued 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("dateIssued")
    public final Builder dateIssued(String dateIssued) {
      this.dateIssued = Preconditions.checkNotNull(dateIssued, "dateIssued");
      initBits &= ~INIT_BIT_DATE_ISSUED;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#dateExpired() dateExpired} attribute.
     * @param dateExpired The value for dateExpired 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("dateExpired")
    public final Builder dateExpired(String dateExpired) {
      this.dateExpired = Preconditions.checkNotNull(dateExpired, "dateExpired");
      initBits &= ~INIT_BIT_DATE_EXPIRED;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#dateAccepted() dateAccepted} attribute.
     * @param dateAccepted The value for dateAccepted 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("dateAccepted")
    public final Builder dateAccepted(String dateAccepted) {
      this.dateAccepted = Preconditions.checkNotNull(dateAccepted, "dateAccepted");
      initBits &= ~INIT_BIT_DATE_ACCEPTED;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#dateCompleted() dateCompleted} attribute.
     * @param dateCompleted The value for dateCompleted 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("dateCompleted")
    public final Builder dateCompleted(String dateCompleted) {
      this.dateCompleted = Preconditions.checkNotNull(dateCompleted, "dateCompleted");
      initBits &= ~INIT_BIT_DATE_COMPLETED;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#numDays() numDays} attribute.
     * @param numDays The value for numDays 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("numDays")
    public final Builder numDays(Integer numDays) {
      this.numDays = Preconditions.checkNotNull(numDays, "numDays");
      initBits &= ~INIT_BIT_NUM_DAYS;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#price() price} attribute.
     * @param price The value for price 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("price")
    public final Builder price(Double price) {
      this.price = Preconditions.checkNotNull(price, "price");
      initBits &= ~INIT_BIT_PRICE;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#reward() reward} attribute.
     * @param reward The value for reward 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("reward")
    public final Builder reward(Double reward) {
      this.reward = Preconditions.checkNotNull(reward, "reward");
      initBits &= ~INIT_BIT_REWARD;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#collateral() collateral} attribute.
     * @param collateral The value for collateral 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("collateral")
    public final Builder collateral(Double collateral) {
      this.collateral = Preconditions.checkNotNull(collateral, "collateral");
      initBits &= ~INIT_BIT_COLLATERAL;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#buyout() buyout} attribute.
     * @param buyout The value for buyout 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("buyout")
    public final Builder buyout(Double buyout) {
      this.buyout = Preconditions.checkNotNull(buyout, "buyout");
      initBits &= ~INIT_BIT_BUYOUT;
      return this;
    }

    /**
     * Initializes the value for the {@link Contract#volume() volume} attribute.
     * @param volume The value for volume 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("volume")
    public final Builder volume(Double volume) {
      this.volume = Preconditions.checkNotNull(volume, "volume");
      initBits &= ~INIT_BIT_VOLUME;
      return this;
    }

    /**
     * Builds a new {@link ImmutableContract ImmutableContract}.
     * @return An immutable instance of Contract
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableContract build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutableContract(
          contractID,
          issuerID,
          issuerCorpID,
          assigneeID,
          acceptorID,
          startStationID,
          endStationID,
          type,
          status,
          title,
          forCorp,
          availability,
          dateIssued,
          dateExpired,
          dateAccepted,
          dateCompleted,
          numDays,
          price,
          reward,
          collateral,
          buyout,
          volume);
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = Lists.newArrayList();
      if ((initBits & INIT_BIT_CONTRACT_I_D) != 0) attributes.add("contractID");
      if ((initBits & INIT_BIT_ISSUER_I_D) != 0) attributes.add("issuerID");
      if ((initBits & INIT_BIT_ISSUER_CORP_I_D) != 0) attributes.add("issuerCorpID");
      if ((initBits & INIT_BIT_ASSIGNEE_I_D) != 0) attributes.add("assigneeID");
      if ((initBits & INIT_BIT_ACCEPTOR_I_D) != 0) attributes.add("acceptorID");
      if ((initBits & INIT_BIT_START_STATION_I_D) != 0) attributes.add("startStationID");
      if ((initBits & INIT_BIT_END_STATION_I_D) != 0) attributes.add("endStationID");
      if ((initBits & INIT_BIT_TYPE) != 0) attributes.add("type");
      if ((initBits & INIT_BIT_STATUS) != 0) attributes.add("status");
      if ((initBits & INIT_BIT_TITLE) != 0) attributes.add("title");
      if ((initBits & INIT_BIT_FOR_CORP) != 0) attributes.add("forCorp");
      if ((initBits & INIT_BIT_AVAILABILITY) != 0) attributes.add("availability");
      if ((initBits & INIT_BIT_DATE_ISSUED) != 0) attributes.add("dateIssued");
      if ((initBits & INIT_BIT_DATE_EXPIRED) != 0) attributes.add("dateExpired");
      if ((initBits & INIT_BIT_DATE_ACCEPTED) != 0) attributes.add("dateAccepted");
      if ((initBits & INIT_BIT_DATE_COMPLETED) != 0) attributes.add("dateCompleted");
      if ((initBits & INIT_BIT_NUM_DAYS) != 0) attributes.add("numDays");
      if ((initBits & INIT_BIT_PRICE) != 0) attributes.add("price");
      if ((initBits & INIT_BIT_REWARD) != 0) attributes.add("reward");
      if ((initBits & INIT_BIT_COLLATERAL) != 0) attributes.add("collateral");
      if ((initBits & INIT_BIT_BUYOUT) != 0) attributes.add("buyout");
      if ((initBits & INIT_BIT_VOLUME) != 0) attributes.add("volume");
      return "Cannot build Contract, some of required attributes are not set " + attributes;
    }
  }
}
