package xyz.notarealtree.suddenly.freight.core.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Generated;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;

/**
 * Immutable implementation of {@link ContractResult}.
 * <p>
 * Use the builder to create immutable instances:
 * {@code ImmutableContractResult.builder()}.
 */
@SuppressWarnings({"all"})
@ParametersAreNonnullByDefault
@Generated({"Immutables.generator", "ContractResult"})
@Immutable
public final class ImmutableContractResult
    implements ContractResult {
  private final LinkedList<HashMap<String, String>> rowset;

  private ImmutableContractResult(LinkedList<HashMap<String, String>> rowset) {
    this.rowset = rowset;
  }

  /**
   * @return The value of the {@code rowset} attribute
   */
  @JsonProperty("rowset")
  @Override
  public LinkedList<HashMap<String, String>> getRowset() {
    return rowset;
  }

  /**
   * Copy the current immutable object by setting a value for the {@link ContractResult#getRowset() rowset} attribute.
   * A shallow reference equality check is used to prevent copying of the same value by returning {@code this}.
   * @param value A new value for rowset
   * @return A modified copy of the {@code this} object
   */
  public final ImmutableContractResult withRowset(LinkedList<HashMap<String, String>> value) {
    if (this.rowset == value) return this;
    LinkedList<HashMap<String, String>> newValue = Preconditions.checkNotNull(value, "rowset");
    return new ImmutableContractResult(newValue);
  }

  /**
   * This instance is equal to all instances of {@code ImmutableContractResult} that have equal attribute values.
   * @return {@code true} if {@code this} is equal to {@code another} instance
   */
  @Override
  public boolean equals(@Nullable Object another) {
    if (this == another) return true;
    return another instanceof ImmutableContractResult
        && equalTo((ImmutableContractResult) another);
  }

  private boolean equalTo(ImmutableContractResult another) {
    return rowset.equals(another.rowset);
  }

  /**
   * Computes a hash code from attributes: {@code rowset}.
   * @return hashCode value
   */
  @Override
  public int hashCode() {
    int h = 5381;
    h += (h << 5) + rowset.hashCode();
    return h;
  }

  /**
   * Prints the immutable value {@code ContractResult} with attribute values.
   * @return A string representation of the value
   */
  @Override
  public String toString() {
    return MoreObjects.toStringHelper("ContractResult")
        .omitNullValues()
        .add("rowset", rowset)
        .toString();
  }

  /**
   * Utility type used to correctly read immutable object from JSON representation.
   * @deprecated Do not use this type directly, it exists only for the <em>Jackson</em>-binding infrastructure
   */
  @Deprecated
  @JsonDeserialize
  @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE)
  static final class Json implements ContractResult {
    @Nullable LinkedList<HashMap<String, String>> rowset;
    @JsonProperty("rowset")
    public void setRowset(LinkedList<HashMap<String, String>> rowset) {
      this.rowset = rowset;
    }
    @Override
    public LinkedList<HashMap<String, String>> getRowset() { throw new UnsupportedOperationException(); }
  }

  /**
   * @param json A JSON-bindable data structure
   * @return An immutable value type
   * @deprecated Do not use this method directly, it exists only for the <em>Jackson</em>-binding infrastructure
   */
  @Deprecated
  @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
  static ImmutableContractResult fromJson(Json json) {
    ImmutableContractResult.Builder builder = ImmutableContractResult.builder();
    if (json.rowset != null) {
      builder.rowset(json.rowset);
    }
    return builder.build();
  }

  /**
   * Creates an immutable copy of a {@link ContractResult} value.
   * Uses accessors to get values to initialize the new immutable instance.
   * If an instance is already immutable, it is returned as is.
   * @param instance The instance to copy
   * @return A copied immutable ContractResult instance
   */
  public static ImmutableContractResult copyOf(ContractResult instance) {
    if (instance instanceof ImmutableContractResult) {
      return (ImmutableContractResult) instance;
    }
    return ImmutableContractResult.builder()
        .from(instance)
        .build();
  }

  /**
   * Creates a builder for {@link ImmutableContractResult ImmutableContractResult}.
   * @return A new ImmutableContractResult builder
   */
  public static ImmutableContractResult.Builder builder() {
    return new ImmutableContractResult.Builder();
  }

  /**
   * Builds instances of type {@link ImmutableContractResult ImmutableContractResult}.
   * Initialize attributes and then invoke the {@link #build()} method to create an
   * immutable instance.
   * <p><em>{@code Builder} is not thread-safe and generally should not be stored in a field or collection,
   * but instead used immediately to create instances.</em>
   */
  @NotThreadSafe
  public static final class Builder {
    private static final long INIT_BIT_ROWSET = 0x1L;
    private long initBits = 0x1L;

    private @Nullable LinkedList<HashMap<String, String>> rowset;

    private Builder() {
    }

    /**
     * Fill a builder with attribute values from the provided {@code ContractResult} instance.
     * Regular attribute values will be replaced with those from the given instance.
     * Absent optional values will not replace present values.
     * @param instance The instance from which to copy values
     * @return {@code this} builder for use in a chained invocation
     */
    public final Builder from(ContractResult instance) {
      Preconditions.checkNotNull(instance, "instance");
      rowset(instance.getRowset());
      return this;
    }

    /**
     * Initializes the value for the {@link ContractResult#getRowset() rowset} attribute.
     * @param rowset The value for rowset 
     * @return {@code this} builder for use in a chained invocation
     */
    @JsonProperty("rowset")
    public final Builder rowset(LinkedList<HashMap<String, String>> rowset) {
      this.rowset = Preconditions.checkNotNull(rowset, "rowset");
      initBits &= ~INIT_BIT_ROWSET;
      return this;
    }

    /**
     * Builds a new {@link ImmutableContractResult ImmutableContractResult}.
     * @return An immutable instance of ContractResult
     * @throws java.lang.IllegalStateException if any required attributes are missing
     */
    public ImmutableContractResult build() {
      if (initBits != 0) {
        throw new IllegalStateException(formatRequiredAttributesMessage());
      }
      return new ImmutableContractResult(rowset);
    }

    private String formatRequiredAttributesMessage() {
      List<String> attributes = Lists.newArrayList();
      if ((initBits & INIT_BIT_ROWSET) != 0) attributes.add("rowset");
      return "Cannot build ContractResult, some of required attributes are not set " + attributes;
    }
  }
}
