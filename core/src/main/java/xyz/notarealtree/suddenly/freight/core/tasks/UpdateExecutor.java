package xyz.notarealtree.suddenly.freight.core.tasks;

import xyz.notarealtree.suddenly.freight.core.apiclient.EveXmlApiResource;
import xyz.notarealtree.suddenly.freight.core.resource.DbResource;
import xyz.notarealtree.suddenly.freight.core.resource.SlackResource;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class UpdateExecutor {

    public UpdateExecutor(EveXmlApiResource xmlApiResource, DbResource dbResource, SlackResource slackResource) {
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ContractApiRequest(xmlApiResource, dbResource),
                0, 1800, TimeUnit.SECONDS);
        scheduledExecutorService.scheduleAtFixedRate(new ContractDeltaCalculation(dbResource, slackResource),
                30, 1800, TimeUnit.SECONDS);
    }

}
