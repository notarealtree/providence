package xyz.notarealtree.suddenly.freight.core.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SlackConfiguration {
    private String botOauthToken;
    private String token;
    private String channel;

    @JsonProperty
    public String getBotOauthToken() {
        return botOauthToken;
    }

    @JsonProperty
    public void setBotOauthToken(String botOauthToken) {
        this.botOauthToken = botOauthToken;
    }

    @JsonProperty
    public String getToken() {
        return token;
    }

    @JsonProperty
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty
    public String getChannel() {
        return channel;
    }

    @JsonProperty
    public void setChannel(String channel) {
        this.channel = channel;
    }
}
