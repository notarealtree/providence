package xyz.notarealtree.suddenly.freight.core.util;

import xyz.notarealtree.suddenly.freight.core.model.Contract;
import xyz.notarealtree.suddenly.freight.core.model.Metadata;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SlackMessageFormatter {

    public static Optional<String> formatNewContractsMessage(List<Contract> contracts, Metadata metadata) {
        List<Contract> outstanding = contracts.stream().filter(contract -> contract.status().equals("Outstanding"))
                .collect(Collectors.toList());

        List<Contract> newContracts = contracts.stream().filter(contract ->
                LocalDateTime.parse(contract.dateIssued(), MongoTypeHelper.eveFormat).compareTo(metadata.getDate()) > 0)
                .collect(Collectors.toList());

        if (newContracts.isEmpty()) {
            return Optional.empty();
        }

        String message = "There " +
                (newContracts.size() == 1 ? "is " : "are ") +
                newContracts.size() +
                "new freight contract " +
                (newContracts.size() == 1 ? ". " : "s. ") +
                "The total volume of outstanding contracts is " +
                outstanding.stream().map(Contract::volume).reduce((d1, d2) -> d1 + d2).get() +
                " m3.";
        return Optional.of(message);
    }
}