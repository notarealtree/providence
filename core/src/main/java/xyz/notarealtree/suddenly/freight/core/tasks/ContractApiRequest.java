package xyz.notarealtree.suddenly.freight.core.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.notarealtree.suddenly.freight.core.apiclient.EveXmlApiResource;
import xyz.notarealtree.suddenly.freight.core.model.Contract;
import xyz.notarealtree.suddenly.freight.core.resource.DbResource;

import java.util.List;

public class ContractApiRequest implements Runnable{
    private static final Logger log = LoggerFactory.getLogger(ContractApiRequest.class);

    private EveXmlApiResource eveXmlApiResource;
    private DbResource dbResource;

    public ContractApiRequest(EveXmlApiResource eveXmlApiResource, DbResource dbResource) {
        this.eveXmlApiResource = eveXmlApiResource;
        this.dbResource = dbResource;
    }

    @Override
    public void run() {
        log.info("Running scheduled API polling task");
        List<Contract> contracts = eveXmlApiResource.getContracts();
        dbResource.insertContracts(contracts);
    }
}
