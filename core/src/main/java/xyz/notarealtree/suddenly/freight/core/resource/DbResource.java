package xyz.notarealtree.suddenly.freight.core.resource;

import xyz.notarealtree.suddenly.freight.core.model.Contract;
import xyz.notarealtree.suddenly.freight.core.model.Metadata;

import java.util.List;

/**
 * Created by Panda on 2017-04-08.
 */
public interface DbResource {
    List<Contract> loadContracts();

    void insertContracts(List<Contract> contracts);

    Metadata loadMetadata();

    void insertMetadata(Metadata metadata);
}
