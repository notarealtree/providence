package xyz.notarealtree.suddenly.freight.core.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServerConfiguration {
    private ApiConfiguration api;
    private DbConfiguration db;
    private SlackConfiguration slack;

    @JsonProperty
    public ApiConfiguration getApi() {
        return api;
    }

    @JsonProperty
    public void setApi(ApiConfiguration api) {
        this.api = api;
    }

    @JsonProperty
    public DbConfiguration getDb() {
        return db;
    }

    @JsonProperty
    public void setDb(DbConfiguration db) {
        this.db = db;
    }

    @JsonProperty
    public SlackConfiguration getSlack() {
        return slack;
    }

    @JsonProperty
    public void setSlack(SlackConfiguration slack) {
        this.slack = slack;
    }
}
