package xyz.notarealtree.suddenly.freight.core.tasks;

import com.google.common.collect.ImmutableList;
import xyz.notarealtree.suddenly.freight.core.configuration.SlackConfiguration;
import xyz.notarealtree.suddenly.freight.core.model.Contract;
import xyz.notarealtree.suddenly.freight.core.resource.DbResource;
import xyz.notarealtree.suddenly.freight.core.resource.SlackResource;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

public class SlashCommandService {
    private final SlackResource slackResource;
    private final DbResource db;
    private final SlackConfiguration configuration;
    private static final List<String> permittedOptions = ImmutableList.of("list");

    public SlashCommandService(SlackResource slackResource, DbResource db, SlackConfiguration configuration) {
        this.slackResource = slackResource;
        this.db = db;
        this.configuration = configuration;
    }

    public Response handleRequest(String slackToken, String username, String text) {
        List<Contract> contracts = db.loadContracts().stream()
                .filter(contract -> contract.status().equals("Outstanding")).collect(Collectors.toList());
        
    }
}
