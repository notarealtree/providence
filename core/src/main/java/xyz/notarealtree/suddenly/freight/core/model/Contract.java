package xyz.notarealtree.suddenly.freight.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.time.LocalDateTime;

/**
 * Created by Panda on 2017-04-07.
 */
@Value.Immutable
@JsonSerialize(as = ImmutableContract.class)
@JsonDeserialize(as = ImmutableContract.class)
public interface Contract {
    Long contractID();
    String issuerID();
    String issuerCorpID();
    String assigneeID();
    String acceptorID();
    String startStationID();
    String endStationID();
    String type();
    String status();
    String title();
    String forCorp();
    String availability();
    String dateIssued();
    String dateExpired();
    String dateAccepted();
    String dateCompleted();
    Integer numDays();
    Double price();
    Double reward();
    Double collateral();
    Double buyout();
    Double volume();
}
