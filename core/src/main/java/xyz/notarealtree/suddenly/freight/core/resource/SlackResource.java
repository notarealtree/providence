package xyz.notarealtree.suddenly.freight.core.resource;

import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import xyz.notarealtree.suddenly.freight.core.configuration.SlackConfiguration;

import javax.ws.rs.client.Client;
import java.io.IOException;

public class SlackResource {
    private final SlackConfiguration slack;
    private final SlackSession session;
    private SlackChannel channel;

    public SlackResource(SlackConfiguration slack) {
        this.slack = slack;
        this.session = SlackSessionFactory.createWebSocketSlackSession(slack.getBotOauthToken());

        try {
            this.session.connect();
            this.channel = session.findChannelByName(slack.getChannel());
            this.session.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessageViaWebhook(String message) {
        try {
            session.connect();
            session.sendMessage(channel, message);
            session.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
