package xyz.notarealtree.suddenly.freight.core.util;

import org.bson.Document;
import xyz.notarealtree.suddenly.freight.core.model.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MongoTypeHelper {
    public static final DateTimeFormatter eveFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    
    public static Contract documentToContract(Document document) {
        return ImmutableContract.builder()
                .acceptorID(document.getString("acceptorID"))
                .assigneeID(document.getString("assigneeID"))
                .availability(document.getString("availability"))
                .buyout(document.getDouble("buyout"))
                .collateral(document.getDouble("collateral"))
                .contractID(document.getLong("contractID"))
                .dateAccepted(document.getString("dateAccepted"))
                .dateCompleted(document.getString("dateCompleted"))
                .dateExpired(document.getString("dateExpired"))
                .dateIssued(document.getString("dateIssued"))
                .endStationID(document.getString("endStationID"))
                .forCorp(document.getString("forCorp"))
                .issuerCorpID(document.getString("issuerCorpID"))
                .issuerID(document.getString("issuerID"))
                .numDays(document.getInteger("numDays"))
                .price(document.getDouble("price"))
                .reward(document.getDouble("reward"))
                .startStationID(document.getString("startStationID"))
                .status(document.getString("status"))
                .title(document.getString("title"))
                .type(document.getString("type"))
                .volume(document.getDouble("volume"))
                .build();
    }

    public static Document contractToDocument(Contract contract) {
        return new Document()
                .append("_id", contract.contractID())
                .append("acceptorID", contract.acceptorID())
                .append("assigneeID", contract.assigneeID())
                .append("availability", contract.availability())
                .append("buyout", contract.buyout())
                .append("collateral", contract.collateral())
                .append("contractID", contract.contractID())
                .append("dateAccepted", contract.dateAccepted())
                .append("dateCompleted", contract.dateCompleted())
                .append("dateExpired", contract.dateExpired())
                .append("dateIssued", contract.dateIssued())
                .append("endStationID", contract.endStationID())
                .append("forCorp", contract.forCorp())
                .append("issuerCorpID", contract.issuerCorpID())
                .append("issuerID", contract.issuerID())
                .append("numDays", contract.numDays())
                .append("price", contract.price())
                .append("reward", contract.reward())
                .append("startStationID", contract.startStationID())
                .append("status", contract.status())
                .append("title", contract.title())
                .append("type", contract.type())
                .append("volume", contract.volume());
    }

    public static Metadata documentToMetadata(Document document) {
        return ImmutableMetadata.builder()
                ._id(document.getString("_id"))
                .date(LocalDateTime.parse(document.getString("date"), eveFormat))
                .build();
    }

    public static Document metadataToDocument(Metadata metadata) {
        return new Document()
                .append("_id", metadata._id())
                .append("date", metadata.getDate().format(eveFormat));
    }
}
