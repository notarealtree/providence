package xyz.notarealtree.suddenly.freight.core.resource;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface FreightService {
    @POST
    @Path("/freight")
    Response makeSlackRequest(
            @FormParam("token") String slackToken,
            @FormParam("user_name") String username,
            @FormParam("text") String text);
}
