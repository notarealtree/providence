package xyz.notarealtree.suddenly.freight.core.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.HashMap;
import java.util.LinkedList;

@Value.Immutable
@JsonDeserialize(as = ImmutableContractResult.class)
public interface ContractResult extends Result{
    LinkedList<HashMap<String, String>> getRowset();
}
