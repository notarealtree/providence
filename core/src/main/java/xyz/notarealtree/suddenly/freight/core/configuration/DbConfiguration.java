package xyz.notarealtree.suddenly.freight.core.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DbConfiguration {
    private String host;
    private int port;
    private String name;

    @JsonProperty
    public String getHost() {
        return host;
    }

    @JsonProperty
    public void setHost(String host) {
        this.host = host;
    }

    @JsonProperty
    public int getPort() {
        return port;
    }

    @JsonProperty
    public void setPort(int port) {
        this.port = port;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }
}
