package xyz.notarealtree.suddenly.freight.core;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import javax.ws.rs.client.Client;
import io.dropwizard.client.JerseyClientBuilder;
import xyz.notarealtree.suddenly.freight.core.apiclient.EveXmlApiResource;
import xyz.notarealtree.suddenly.freight.core.configuration.FreightConfiguration;
import xyz.notarealtree.suddenly.freight.core.resource.*;
import xyz.notarealtree.suddenly.freight.core.tasks.UpdateExecutor;

public class FreightServer extends Application<FreightConfiguration> {

    public void run(FreightConfiguration configuration, Environment environment) throws Exception {
        final Client client = new JerseyClientBuilder(environment).using(configuration.getJerseyClientConfiguration())
                .build(getName());
        DbResource dbResource = new MongoDbResource(configuration.getServer().getDb());
        EveXmlApiResource apiResource = new EveXmlApiResource(client, configuration.getServer().getApi());
        FreightService freightResource = new FreightResource();
        SlackResource slackResource = new SlackResource(configuration.getServer().getSlack());
        new UpdateExecutor(apiResource, dbResource, slackResource);
        environment.jersey().register(freightResource);
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0){
            args = new String[]{"server", "freight.yml"};
        }
        new FreightServer().run(args);
    }
}
