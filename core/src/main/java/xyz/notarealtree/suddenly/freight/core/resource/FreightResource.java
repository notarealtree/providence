package xyz.notarealtree.suddenly.freight.core.resource;

import xyz.notarealtree.suddenly.freight.core.tasks.SlashCommandService;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.Response;

public class FreightResource implements FreightService{
    private SlashCommandService service;

    public FreightResource (SlashCommandService service) {
        this.service = service;
    }

    @Override
    public Response makeSlackRequest(@FormParam("token") String slackToken,
                                     @FormParam("user_name") String username,
                                     @FormParam("text") String text) {
        return service.handleRequest(slackToken, username, text);
    }
}
