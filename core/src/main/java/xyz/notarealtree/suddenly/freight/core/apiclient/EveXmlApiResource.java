package xyz.notarealtree.suddenly.freight.core.apiclient;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import xyz.notarealtree.suddenly.freight.core.configuration.ApiConfiguration;
import xyz.notarealtree.suddenly.freight.core.model.*;
import xyz.notarealtree.suddenly.freight.core.util.MongoTypeHelper;

import javax.ws.rs.client.Client;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class EveXmlApiResource {
    private final Client client;
    private final String keyId;
    private final String vCode;

    public EveXmlApiResource(Client client, ApiConfiguration api) {
        this.client = client;
        this.keyId = api.getKeyId();
        this.vCode = api.getvCode();
    }

    public List<Contract> getContracts() {
        String response = client
                .target("https://api.eveonline.com")
                .path("/corp/Contracts.xml.aspx")
                .queryParam("keyID", keyId)
                .queryParam("vCode", vCode)
                .queryParam("rowCount", "2560")
                .request()
                .get(String.class);

        final TypeReference type = new TypeReference<EveApi<ContractResult>>() {};
        EveApi eveApi;
        try {
            eveApi = new XmlMapper().readValue(response, type);
        } catch (IOException e) {
            throw new RuntimeException("Parsing contracts failed with " + e);
        }

        ContractResult result = (ContractResult) eveApi.getResult();
        return result.getRowset().stream().map(row -> {
            return ImmutableContract.builder()
                    .acceptorID(row.get("acceptorID"))
                    .assigneeID(row.get("assigneeID"))
                    .availability(row.get("availability"))
                    .buyout(Double.valueOf(row.get("buyout")))
                    .collateral(Double.valueOf(row.get("collateral")))
                    .contractID(Long.valueOf(row.get("contractID")))
                    .dateAccepted(row.get("dateAccepted"))
                    .dateCompleted(row.get("dateCompleted"))
                    .dateExpired(row.get("dateExpired"))
                    .dateIssued(row.get("dateIssued"))
                    .endStationID(row.get("endStationID"))
                    .forCorp(row.get("forCorp"))
                    .issuerCorpID(row.get("issuerCorpID"))
                    .issuerID(row.get("issuerID"))
                    .numDays(Integer.valueOf(row.get("numDays")))
                    .price(Double.valueOf(row.get("price")))
                    .reward(Double.valueOf(row.get("reward")))
                    .startStationID(row.get("startStationID"))
                    .status(row.get("status"))
                    .title(row.get("title"))
                    .type(row.get("type"))
                    .volume(Double.valueOf(row.get("volume")))
                    .build();
        }).collect(Collectors.toList());
    }
}
