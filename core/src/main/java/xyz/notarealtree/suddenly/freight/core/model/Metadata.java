package xyz.notarealtree.suddenly.freight.core.model;

import org.immutables.value.Value;

import java.time.LocalDateTime;

@Value.Immutable
public interface Metadata {
    String _id();
    LocalDateTime getDate();
}
