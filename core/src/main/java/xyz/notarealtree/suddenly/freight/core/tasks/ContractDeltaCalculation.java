package xyz.notarealtree.suddenly.freight.core.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.notarealtree.suddenly.freight.core.model.Contract;
import xyz.notarealtree.suddenly.freight.core.model.Metadata;
import xyz.notarealtree.suddenly.freight.core.resource.DbResource;
import xyz.notarealtree.suddenly.freight.core.resource.SlackResource;
import xyz.notarealtree.suddenly.freight.core.util.SlackMessageFormatter;

import java.util.List;
import java.util.stream.Collectors;

public class ContractDeltaCalculation implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(ContractDeltaCalculation.class);
    private DbResource dbResource;
    private SlackResource slackResource;

    public ContractDeltaCalculation(DbResource dbResource, SlackResource slackResource) {
        this.dbResource = dbResource;
        this.slackResource = slackResource;
    }

    @Override
    public void run() {
        log.info("Running scheduled Delta calculation task");
        List<Contract> contracts = dbResource.loadContracts().stream()
                .filter(contract -> contract.type().equals("Courier") && contract.assigneeID().equals("98215836"))
                .collect(Collectors.toList());
        Metadata metadata = dbResource.loadMetadata();
        SlackMessageFormatter.formatNewContractsMessage(contracts, metadata)
                .ifPresent(message -> slackResource.sendMessageViaWebhook(message));
    }
}
