package xyz.notarealtree.suddenly.freight.core.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiConfiguration {
    private String keyId;
    private String vCode;

    @JsonProperty
    public String getKeyId() {
        return keyId;
    }

    @JsonProperty
    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    @JsonProperty
    public String getvCode() {
        return vCode;
    }

    @JsonProperty
    public void setvCode(String vCode) {
        this.vCode = vCode;
    }
}
