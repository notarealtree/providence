package xyz.notarealtree.suddenly.freight.core.resource;

import com.google.common.collect.Lists;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import xyz.notarealtree.suddenly.freight.core.configuration.DbConfiguration;
import xyz.notarealtree.suddenly.freight.core.model.Contract;
import xyz.notarealtree.suddenly.freight.core.model.Metadata;
import xyz.notarealtree.suddenly.freight.core.util.MongoTypeHelper;

import java.util.List;
import java.util.function.Consumer;

public class MongoDbResource implements DbResource {
    private MongoCollection<Document> contractCollection;
    private MongoCollection<Document> metadataCollection;

    public MongoDbResource(DbConfiguration db) {
        MongoClient mongoClient = new MongoClient(db.getHost(), db.getPort());
        contractCollection = mongoClient
                .getDatabase(db.getName())
                .getCollection("contracts");
        metadataCollection = mongoClient
                .getDatabase(db.getName())
                .getCollection("metadata");
    }

    @Override
    public List<Contract> loadContracts() {
        List<Contract> contracts = Lists.newLinkedList();
        contractCollection.find().map(MongoTypeHelper::documentToContract)
                .forEach((Consumer<? super Contract>) contracts::add);
        return contracts;
    }

    @Override
    public void insertContracts(List<Contract> contracts) {
        UpdateOptions options = new UpdateOptions().upsert(true);
        contracts.stream().map(MongoTypeHelper::contractToDocument)
                .forEach(document -> {
                    contractCollection.replaceOne(Filters.eq("_id", document.get("_id")), document, options);
                });
    }

    @Override
    public Metadata loadMetadata() {
        return metadataCollection.find(Filters.eq("_id", "1")).map(MongoTypeHelper::documentToMetadata).first();
    }

    @Override
    public void insertMetadata(Metadata metadata) {
        metadataCollection.replaceOne(Filters.eq("_id", "1"), MongoTypeHelper.metadataToDocument(metadata));
    }
}
