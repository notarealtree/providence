package xyz.notarealtree.suddenly.freight.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EveApi<T extends Result> {
    private T result;

    public EveApi() {}

    public Result getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
